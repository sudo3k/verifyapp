import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerifyaccountComponent } from './verifyaccount/verifyaccount.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'verifyAccount/:data', component: VerifyaccountComponent },
  { path: 'login', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
  NgxSpinnerModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
