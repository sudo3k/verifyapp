import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";



@Component({
  selector: 'app-verifyaccount',
  templateUrl: './verifyaccount.component.html',
  styleUrls: ['./verifyaccount.component.scss']
})
export class VerifyaccountComponent implements OnInit {
  data:any;

  constructor(private act:ActivatedRoute,  private spinner:NgxSpinnerService) { 
    this.spinner.show();
    this.data = this.act.snapshot.params.data
    this.data = JSON.parse(this.data)
    console.log(this.data)
    
    setTimeout(()=>{this.spinner.hide()}, 5000)
  }

  ngOnInit(): void {
   
  }

}
